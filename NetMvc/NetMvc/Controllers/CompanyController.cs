﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetMvc.Models;

namespace NetMvc.Controllers
{
    public class CompanyController : Controller
    {
        private static List<CompanyViewModel> _companyViewModels = new List<CompanyViewModel>()
        {
            new CompanyViewModel(1, "Astra Graphia Teknologi", "logoAstraG.jpg", "Jl Bandung", 02132147747),
            new CompanyViewModel(2, "Telkomsel", "logoTsel.jpg", "Jl Gatot Subroto", 02144377789),
            new CompanyViewModel(3, "Astra Otoparts", "logoAstraO.jpg", "Jl Jakrata 2", 0213232999),
            new CompanyViewModel(4, "PUPR", "logoPupr.jpg", "Jl Presiden", 02155511222),
            new CompanyViewModel(5, "Pertamina", "logoPertamina.jpg", "Jl Kemerdekaan", 02188877711),
        };

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, CompanyName, Logo, Address, Telp")] CompanyViewModel company)
        {
            _companyViewModels.Add(company);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_companyViewModels);
        }

        public IActionResult Edit(int? id)
        {
            // find with lambda
            CompanyViewModel company = _companyViewModels.Find(x => x.Id.Equals(id));
            return View(company);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, CompanyName, Logo, Address, Telp")] CompanyViewModel company)
        {
            // hapus data lama
            CompanyViewModel companyOld = _companyViewModels.Find(x => x.Id.Equals(id));
            _companyViewModels.Remove(companyOld);

            // input data baru
            _companyViewModels.Add(company);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            // find with linq
            CompanyViewModel company = (
                    from p in _companyViewModels
                    where p.Id.Equals(id)
                    select p
                ).SingleOrDefault(new CompanyViewModel());
            return View(company);
        }


        public IActionResult Delete(int? id)
        {
            // find data dulu
            CompanyViewModel company = _companyViewModels.Find(x => x.Id.Equals(id));
            // remove from list
            _companyViewModels.Remove(company);

            return RedirectToAction(nameof(List));
        }
    }
}
