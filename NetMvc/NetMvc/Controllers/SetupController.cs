﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetMvc.Models;
using System.Globalization;

namespace NetMvc.Controllers
{
    public class SetupController : Controller
    {
        private static List<SetupViewModel> _setupViewModels = new List<SetupViewModel>()
        {
            new SetupViewModel(1, "Agus Budiono", "15-10-1999", "15-10-1999" , "AGIT", "James", "Dodi"),
            new SetupViewModel(2, "Siti Kholifah", "15-10-1999", "15-10-1999" , "AGIT", "James", "Dodi"),
            new SetupViewModel(3, "Banu Anugrah", "15-10-1999", "15-10-1999" , "AGIT", "James", "Dodi"),
            new SetupViewModel(4, "Danang Setiawan", "15-10-1999", "15-10-1999" , "AGIT", "James", "Dodi"),
            new SetupViewModel(5, "Rizki Jamaludin", "15-10-1999", "15-10-1999" , "AGIT", "James", "Dodi"),
        };

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, FullName, StartDate, EndDate, Company, ReportTo1, ReportTo2")] SetupViewModel setup)
        {
            _setupViewModels.Add(setup);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_setupViewModels);
        }

        public IActionResult Edit(int? id)
        {
            // find with lambda
            SetupViewModel setup = _setupViewModels.Find(x => x.Id.Equals(id));
            return View(setup);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, FullName, StartDate, EndDate, Company, ReportTo1, ReportTo2")] SetupViewModel setup)
        {
            // hapus data lama
            SetupViewModel setupOld = _setupViewModels.Find(x => x.Id.Equals(id));
            _setupViewModels.Remove(setupOld);

            // input data baru
            _setupViewModels.Add(setup);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            // find with linq
            SetupViewModel setup = (
                    from p in _setupViewModels
                    where p.Id.Equals(id)
                    select p
                ).SingleOrDefault(new SetupViewModel());
            return View(setup);
        }


        public IActionResult Delete(int? id)
        {
            // find data dulu
            SetupViewModel setup = _setupViewModels.Find(x => x.Id.Equals(id));
            // remove from list
            _setupViewModels.Remove(setup);

            return RedirectToAction(nameof(List));
        }
    }
}
