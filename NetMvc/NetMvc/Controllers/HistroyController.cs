﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetMvc.Models;
using System.Globalization;

namespace NetMvc.Controllers
{
    public class HistoryController : Controller
    {
        private static List<HistoryViewModel> _historyViewModels = new List<HistoryViewModel>()
        {
            new HistoryViewModel(1, "excelreport1.xlsx", "Data Report Desember", "15-10-1999", "" , "James", "Dodi"),
            new HistoryViewModel(2, "yuliGenerate.pdf", "Yuli Generate", "", "15-10-1999" , "James", "Dodi"),
            new HistoryViewModel(3, "agusGenerate.pdf", "Agus Generate", "", "15-10-1999" , "James", "Dodi"),
            new HistoryViewModel(4, "excelreport2.xlsx", "Data Report Januari", "15-10-1999", "" , "James", "Dodi"),
            new HistoryViewModel(5, "calvinGenerate.pdf", "Calvin Generate", "", "15-10-1999" , "James", "Dodi"),
        };

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, FileName, Descirption, DateUpload, DateGenerate, UploadBy, GenerateBy")] HistoryViewModel history)
        {
            _historyViewModels.Add(history);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_historyViewModels);
        }

        public IActionResult Edit(int? id)
        {
            // find with lambda
            HistoryViewModel history = _historyViewModels.Find(x => x.Id.Equals(id));
            return View(history);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, FileName, Descirption, DateUpload, DateGenerate, UploadBy, GenerateBy")] HistoryViewModel history)
        {
            // hapus data lama
            HistoryViewModel historyOld = _historyViewModels.Find(x => x.Id.Equals(id));
            _historyViewModels.Remove(historyOld);

            // input data baru
            _historyViewModels.Add(history);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            // find with linq
            HistoryViewModel history = (
                    from p in _historyViewModels
                    where p.Id.Equals(id)
                    select p
                ).SingleOrDefault(new HistoryViewModel());
            return View(history);
        }


        public IActionResult Delete(int? id)
        {
            // find data dulu
            HistoryViewModel history = _historyViewModels.Find(x => x.Id.Equals(id));
            // remove from list
            _historyViewModels.Remove(history);

            return RedirectToAction(nameof(List));
        }
    }
}
