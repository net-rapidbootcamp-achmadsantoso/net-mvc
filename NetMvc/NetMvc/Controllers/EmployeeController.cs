﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetMvc.Models;

namespace NetMvc.Controllers
{
    public class EmployeeController : Controller
    {
        private static List<EmployeeViewModel> _employeeViewModels = new List<EmployeeViewModel>()
        {
            new EmployeeViewModel(1, "Agus Jaya", "agsuagus@mail.com", noHp: 0872182927733, "Jl Bandung", "Programmer"),
            new EmployeeViewModel(2, "David Gadgetin", "david2@mail.com", noHp: 081316345831, "Jl Jakarta 1", "Manajer"),
            new EmployeeViewModel(3, "Bewok", "bewok2@mail.com", noHp: 085837299809, "Jl Apel 99", "Programmer"),
            new EmployeeViewModel(4, "Fajar Sad Boy", "fajar66@mail.com", noHp: 088777721111, "Jl Ketokprak", "HR"),
            new EmployeeViewModel(5, "Deddy Mahendra", "deddyo@mail.com", noHp: 081334567890, "Jl Anggur", "Finance"),
        };

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, FullName, Email, NoHp, Address, Position")] EmployeeViewModel employee)
        {
            _employeeViewModels.Add(employee);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_employeeViewModels);
        }

        public IActionResult Edit(int? id)
        {
            // find with lambda
            EmployeeViewModel product = _employeeViewModels.Find(x => x.Id.Equals(id));
            return View(product);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, FullName, Email, NoHp, Address, Position")] EmployeeViewModel product)
        {
            // hapus data lama
            EmployeeViewModel productOld = _employeeViewModels.Find(x => x.Id.Equals(id));
            _employeeViewModels.Remove(productOld);

            // input data baru
            _employeeViewModels.Add(product);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            // find with linq
            EmployeeViewModel product = (
                    from p in _employeeViewModels
                    where p.Id.Equals(id)
                    select p
                ).SingleOrDefault(new EmployeeViewModel());
            return View(product);
        }


        public IActionResult Delete(int? id)
        {
            // find data dulu
            EmployeeViewModel productt = _employeeViewModels.Find(x => x.Id.Equals(id));
            // remove from list
            _employeeViewModels.Remove(productt);

            return RedirectToAction(nameof(List));
        }
    }
}
