﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetMvc.Models;
using System.Globalization;

namespace NetMvc.Controllers
{
    public class ReportController : Controller
    {
        private static List<ReportViewModel> _reportViewModels = new List<ReportViewModel>()
        {
            new ReportViewModel(1, "Agus Budiono", "1999-01-17" , "Work", "08.00", "17.00", "Develop Form Employee"),
            new ReportViewModel(2, "Siti Kholifah","1999-01-17" , "Work", "08.00", "17.00", "Develop Form Employee"),
            new ReportViewModel(3, "Banu Anugrah", "1999-01-17" , "Work", "08.00", "17.00", "Develop Form Employee"),
            new ReportViewModel(4, "Danang Setiawan", "1999-01-17" , "Work", "08.00", "17.00", "Develop Form Employee"),
            new ReportViewModel(5, "Rizki Jamaludin", "1999-01-17" , "Work", "08.00", "17.00", "Develop Form Employee"),
        };

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Save([Bind("Id, FullName, Date, Shift, In, Out, Task")] ReportViewModel report)
        {
            _reportViewModels.Add(report);
            return Redirect("List");
        }

        public IActionResult List()
        {
            return View(_reportViewModels);
        }

        public IActionResult Edit(int? id)
        {
            // find with lambda
            ReportViewModel report = _reportViewModels.Find(x => x.Id.Equals(id));
            return View(report);
        }

        [HttpPost]
        public IActionResult Update(int id, [Bind("Id, FullName, Date, Shift, In, Out, Task")] ReportViewModel report)
        {
            // hapus data lama
            ReportViewModel reportOld = _reportViewModels.Find(x => x.Id.Equals(id));
            _reportViewModels.Remove(reportOld);

            // input data baru
            _reportViewModels.Add(report);
            return Redirect("List");
        }

        public IActionResult Details(int id)
        {
            // find with linq
            ReportViewModel report = (
                    from p in _reportViewModels
                    where p.Id.Equals(id)
                    select p
                ).SingleOrDefault(new ReportViewModel());
            return View(report);
        }


        public IActionResult Delete(int? id)
        {
            // find data dulu
            ReportViewModel report = _reportViewModels.Find(x => x.Id.Equals(id));
            // remove from list
            _reportViewModels.Remove(report);

            return RedirectToAction(nameof(List));
        }
    }
}
