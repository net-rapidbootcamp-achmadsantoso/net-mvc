﻿namespace NetMvc.Models;

public class SetupViewModel
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public string StartDate { get; set; }
    public string EndDate { get; set; }
    public string Company { get; set; }
    public string ReportTo1 { get; set; }
    public string ReportTo2 { get; set; }

    public SetupViewModel(int id, string fullName, string startDate, string endDate, string company, string reportTo1, string reportTo2)
    {
        Id = id;
        FullName = fullName;
        StartDate = startDate;
        EndDate = endDate;
        Company = company;
        ReportTo1 = reportTo1;
        ReportTo2 = reportTo2;
    }

    public SetupViewModel()
    {
    }
}

