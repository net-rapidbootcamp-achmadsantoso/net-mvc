﻿namespace NetMvc.Models;

public class CompanyViewModel
{
    public int Id { get; set; }
    public String CompanyName { get; set; }
    public String Logo { get; set; }
    public string Address { get; set; }
    public long Telp { get; set; }

    public CompanyViewModel(int id, string companyName, string logo, string address, long telp)
    {
        Id = id;
        CompanyName = companyName;
        Logo = logo;
        Address = address;
        Telp = telp;
    }

    public CompanyViewModel()
    {
    }
}

