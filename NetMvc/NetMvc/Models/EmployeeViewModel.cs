﻿namespace NetMvc.Models;

public class EmployeeViewModel
{
    public int Id { get; set; }
    public String FullName { get; set; }
    public String Email { get; set; }
    public long NoHp { get; set; }
    public string Address { get; set; }
    public String Position { get; set; }

    public EmployeeViewModel(int id, string fullName, string email, long noHp, string address, string position)
    {
        Id = id;
        FullName = fullName;
        Email = email;
        NoHp = noHp;
        Address = address;
        Position = position;
    }

    public EmployeeViewModel()
    {
    }
}

