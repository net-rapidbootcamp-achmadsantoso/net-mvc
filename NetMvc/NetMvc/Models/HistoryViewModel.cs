﻿namespace NetMvc.Models;

public class HistoryViewModel
{
    public int Id { get; set; }
    public string FileName { get; set; }
    public string Description { get; set; }
    public string DateUpload { get; set; }
    public string DateGenerate { get; set; }
    public string UploadBy { get; set; }
    public string GenerateBy { get; set; }

    public HistoryViewModel(int id, string fileName, string description, string dateUpload, string dateGenerate, string uploadBy, string generateBy)
    {
        Id = id;
        FileName = fileName;
        Description = description;
        DateUpload = dateUpload;
        DateGenerate = dateGenerate;
        UploadBy = uploadBy;
        GenerateBy = generateBy;
    }

    public HistoryViewModel()
    {
    }
}

