﻿namespace NetMvc.Models;

public class ReportViewModel
{
    public int Id { get; set; }
    public string FullName { get; set; }
    public string Date { get; set; }
    public string Shift { get; set; }
    public string In { get; set; }
    public string Out { get; set; }
    public string Task { get; set; }

    public ReportViewModel(int id, string fullName, string date, string shift, string clockIn, string clockOut, string task)
    {
        Id = id;
        FullName = fullName;
        Date = date;
        Shift = shift;
        In = clockIn;
        Out = clockOut;
        Task = task;
    }

    public ReportViewModel()
    {
    }
}

